from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def memories(request):
    return render(request, 'memories.html')

def smt3(request):
    return render(request, 'smt3.html')